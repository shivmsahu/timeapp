part of 'time_bloc.dart';

@immutable
abstract class TimeEvent {}

class MessageEvent extends TimeEvent {
  DateTime time;
  MessageEvent(this.time);
}
