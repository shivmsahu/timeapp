part of 'time_bloc.dart';

@immutable
abstract class TimeState {}

class TimeInitial extends TimeState {}

class MessageState extends TimeState {
  var message;
  DateTime time;
  MessageState(this.time, this.message);
}
