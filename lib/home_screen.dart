import 'package:flutter/material.dart';
import 'bloc/time_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _bloc = BlocProvider.of<TimeBloc>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text("Time App"),
          backgroundColor: Colors.redAccent,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<TimeBloc, TimeState>(
                builder: (context, state) {
                  _bloc.add(MessageEvent(DateTime.now()));
                  return Column(
                    children: [
                      Text(
                        (state is MessageState)
                            ? '${state.time.hour.toString().padLeft(2, '0')}:${state.time.minute.toString().padLeft(2, '0')}:${state.time.second.toString().padLeft(2, '0')}'
                            : 'Loading...',
                        style: TextStyle(
                          fontSize: 80,
                        ),
                      ),
                      Text(
                        (state is MessageState)
                            ? 'Hey! Good ${state.message}'
                            : '',
                        style: TextStyle(
                          fontSize: 40,
                        ),
                      ),
                    ],
                  );
                },
              ),
              // RaisedButton(onPressed: () {
              //   _bloc.add(MessageEvent(DateTime.now()));
              // }),
            ],
          ),
        ));
  }
}
